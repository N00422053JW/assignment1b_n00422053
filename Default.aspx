﻿<%@ Page Language="C#" Inherits="assignment_1b.Default" %>
<!DOCTYPE html>
<html>
<head runat="server">
	<title>Default</title>
</head>
<body>
    <form id="form1" runat="server">
        <div>
            <h1>Babysitting Service</h1>
            <br/>
            <asp:ValidationSummary id="ValidationSummary" Runat="server" />
            Parent's name:
            <asp:TextBox id="clientName" Placeholder="Enter here" runat="server" />
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a name." ControlToValidate="clientName" id="validatorName"></asp:RequiredFieldValidator>
            <br/>
            <br/>
            Parent's phone number:
            <asp:TextBox id="clientPhone" Placeholder="Enter here" runat="server" />
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a phone number." ControlToValidate="clientPhone" id="validatorphone"></asp:RequiredFieldValidator>
            <asp:RegularExpressionValidator ID="clientphonevalid" runat="server" Display="Dynamic" ControlToValidate="clientPhone" ErrorMessage="Please enter phone number." ValidationExpression="^\d+$"></asp:RegularExpressionValidator>
            <br/>
            Please confirm your phone number again:
            <asp:TextBox runat="server" id="clientphoneCompare" Placeholder="Enter here" />
            <asp:RequiredFieldValidator runat="server" ErrorMessage="Please enter a phone number." ControlToValidate="clientphoneCompare" id="validatorphoneCompare"></asp:RequiredFieldValidator>
            <asp:CompareValidator runat="server" id="clientphoneComparevalid" Display="Dynamic" Controltovalidate="clientphoneCompare" controltocompare="clientphone" operator="Equal" type="Integer" errormessage="Incorrect number." />
            <br/>
            <br/>
            Number of kids for babysitting (max.5):
            <asp:TextBox id="numberOfKids" Placeholder="Enter a number" runat="server" />
            <asp:RangeValidator runat="server" id="numberOfKidsValid" ControlToValidate="numberOfKids" Type="Integer" MinimumValue="1" MaximumValue="5" ErrorMessage="Enter a number (max. 5 kids)"></asp:RangeValidator>
            <br/>
            <br/>
            Kid's age:
            <div id="kidsAge" runat="server">
                <asp:CheckBox runat="server" id="ageRange1" Text="2-3 years old"/>
                <asp:CheckBox runat="server" id="ageRange2" Text="4-6 years old"/>
                <asp:CheckBox runat="server" id="ageRange3" Text="7-10 years old"/>
                <asp:CheckBox runat="server" id="ageRange4" Text="10-13 years old"/>
                </div>
            <br/>
            Parent's home address:
            <asp:TextBox id="clientAddress" Placeholder="Enter here" runat="server" />
            <asp:RequiredFieldValidator runat="server" ControlToValidate="clientAddress" ErrorMessage="Please enter an address."></asp:RequiredFieldValidator>
            <br/>
            Choose one:
            <br/>
            <asp:RadioButtonList runat="server" id="orderType">
                 <asp:ListItem Text="Drop Off">Drop Off</asp:ListItem>
                 <asp:ListItem Text="Pick Up">Pick Up</asp:ListItem>
            </asp:RadioButtonList>
            <br/>
            Time for service:
            <br/>
            <asp:RadioButtonList runat="server" id="serviceTime">
                 <asp:ListItem Text="8am-12pm">8am-12pm</asp:ListItem>
                 <asp:ListItem Text="1pm-5pm">1pm-5pm</asp:ListItem>
                 <asp:ListItem Text="6pm-11pm">6pm-10pm</asp:ListItem>
            </asp:RadioButtonList>
            <br/>
            Choice of meal:
            <asp:DropDownList runat="server" id="meal">
                <asp:ListItem Value="Chicken" Text="Chicken"></asp:ListItem>
                <asp:ListItem Value="Beef" Text="Beef"></asp:ListItem>
                <asp:ListItem Value="Fish" Text="Fish"></asp:ListItem>
                <asp:ListItem Value="Vegetarian" Text="Vegetarian"></asp:ListItem>
                </asp:DropDownList>
            <br/>
            Choice of snack:
            <asp:DropDownList runat="server" id="snack">
                <asp:ListItem Value="Cheese" Text="Cheese"></asp:ListItem>
                <asp:ListItem Value="Fruit" Text="Fruit"></asp:ListItem>
                <asp:ListItem Value="Chocolate" Text="Chocolate"></asp:ListItem>
                <asp:ListItem Value="Cookies" Text="Cookies"></asp:ListItem>
                </asp:DropDownList>
            <br/>
            <br/>
            Speical request (optional):
            <br/>
            <asp:TextBox id="specialRequest" TextMode="multiline" Placeholder="e.g. Allergies"runat="server"/>
            <br/>
            <br/>
            <asp:Button runat="server" id="submitform" Text="Submit" OnClick="Submit"/>
            <br/>
            <div runat="server" id="sumup"></div>
         </div>
    </form>
</body>
</html>

