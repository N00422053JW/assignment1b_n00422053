﻿using System;
namespace assignment_1b
{
    public class Order
    {
        public Parent parent;
        public Service service;

        public Order(Parent p, Service s)
        {
            parent = p;
            service = s;
        }

        public string PrintReceipt()
        {
            string receipt = "Thank you for your order!<br>Receipt:<br>";

            receipt += "Name: " + parent.ParentName + "<br/>";
            receipt += "Your total is: $" + CalculateOrder().ToString() + " for "+service.numberOfKids.ToString()+" kids <br/>";
            receipt += "Phone Number: " + parent.ParentPhone + "<br/>";
            receipt += "Address: " + parent.ParentAddress + "<br/>";
            receipt += "" + service.orderType + "<br/>";
            receipt += "Kid's age: " + String.Join(" & ", service.kidsAge.ToArray()) + "<br/>";
            receipt += "Time for Service: " + service.stime + "<br/>";
            receipt += "Choice of meal: " + service.meal + "<br/>";
            receipt += "Choice of snack: " + service.snack +"<br/>";
            receipt += "Special request(optional): " + service.SpecialRequest + "<br/>";

            return receipt;
        }

       public double CalculateOrder()
        {
            double total = 0;

            total += service.numberOfKids * 40;

            return total;
        }
    }
}
