﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
namespace assignment_1b
{
    public class Service
    {
        public int numberOfKids;
        public List<string> kidsAge;
        public string stime;
        public string orderType;
        public string meal;
        public string snack;
        public string specialRequest;


        public Service(List<string> ka, string st, string ot, string ml, string sn)
        {
            kidsAge = ka;
            stime = st;
            orderType = ot;
            meal = ml;
            snack = sn;
        }

        public int NumberOfKids
        {
            get { return numberOfKids; }
            set { numberOfKids = value; }
        }

        public string SpecialRequest
        {
            get { return specialRequest; }
            set { specialRequest = value; }
        }

    }
}
