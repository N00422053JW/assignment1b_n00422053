﻿using System;
namespace assignment_1b
{
    public class Parent
    {
        private string parentName;
        private string parentPhone;
        private string parentAddress;

        public Parent()
        {
        }

        public string ParentName
        {
            get { return parentName; }
            set { parentName = value; }
        }
        public string ParentPhone
        {
            get { return parentPhone; }
            set { parentPhone = value; }
        }
        public string ParentAddress
        {
            get { return parentAddress; }
            set { parentAddress = value; }
        }
    }
}
