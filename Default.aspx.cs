﻿using System;
using System.Collections.Generic;
using System.Globalization;
using System.Linq;
using System.Web;
using System.Web.UI;
using System.Web.UI.WebControls;

namespace assignment_1b
{

    public partial class Default : System.Web.UI.Page
    {
        protected void Submit(object sender, EventArgs e)
        {
            string name = clientName.Text.ToString();
            string phone = clientPhone.Text.ToString();
            string address = clientAddress.Text.ToString();

            Parent newparent = new Parent();
            newparent.ParentName = name;
            newparent.ParentPhone = phone;
            newparent.ParentAddress = address;


            List<string> kage = new List<string> ();
            // get this from pizza sample - topping
            foreach (Control control in kidsAge.Controls)
            {
                if (control.GetType() == typeof(CheckBox))
                {
                    CheckBox kidsAge_check = (CheckBox)control;
                    if (kidsAge_check.Checked)
                    {
                        kage.Add(kidsAge_check.Text);
                    }

                }
            }

            string stime = serviceTime.SelectedItem.Value.ToString();
            string orty = orderType.SelectedItem.Value.ToString();
            string mealCho = meal.SelectedItem.Value.ToString();
            string snackCho = snack.SelectedItem.Value.ToString();

            int numbKids = int.Parse(numberOfKids.Text);
            string request = specialRequest.Text.ToString();

            Service newservice = new Service(kage, stime, orty, mealCho, snackCho);
            newservice.NumberOfKids = numbKids;
            newservice.SpecialRequest = request;


            Order neworder = new Order(newparent, newservice);
            sumup.InnerHtml = neworder.PrintReceipt();

        }
    }
}
